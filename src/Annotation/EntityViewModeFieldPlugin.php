<?php

namespace Drupal\entity_view_mode_field_plugin\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Entity view mode field plugin item annotation object.
 *
 * @see \Drupal\entity_view_mode_field_plugin\Plugin\EntityViewModeFieldPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class EntityViewModeFieldPlugin extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Entity type can be use the plugin.
   *
   * Empty mean all entity type can use.
   *
   * @var array
   */
  public $entity_type = [];

  /**
   * The field key to get value.
   *
   * @var string
   */
  public $field_key;
}
