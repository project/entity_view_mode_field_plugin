<?php

namespace Drupal\entity_view_mode_field_plugin\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Base class for Entity view mode field plugin plugins.
 */
abstract class EntityViewModeFieldPluginBase extends PluginBase implements EntityViewModeFieldPluginInterface {

  /**
   * Get value for field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Drupal\Core\Entity\EntityInterface.
   *
   * @return mixed
   *   Value.
   */
  public function getValue(EntityInterface $entity) {
    return [];
  }

}
