<?php

namespace Drupal\entity_view_mode_field_plugin\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Entity view mode field plugin plugins.
 */
interface EntityViewModeFieldPluginInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.

}
