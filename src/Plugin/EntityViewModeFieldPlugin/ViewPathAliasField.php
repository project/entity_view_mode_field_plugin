<?php

namespace Drupal\entity_view_mode_field_plugin\Plugin\EntityViewModeFieldPlugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_view_mode_field_plugin\Plugin\EntityViewModeFieldPluginBase;

/**
 * Show Id in entity view mode.
 *
 * @EntityViewModeFieldPlugin(
 *   id = "path_alias",
 *   label = @Translation("URL Alias"),
 *   entity_type = {
 *     "node"
 *   }
 * )
 */
class ViewPathAliasField extends EntityViewModeFieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getValue(EntityInterface $entity) {
    return \Drupal::service('path_alias.manager')
      ->getAliasByPath('/node/' . $entity->id());
  }

}
