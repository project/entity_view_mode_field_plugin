<?php

namespace Drupal\entity_view_mode_field_plugin\Plugin\EntityViewModeFieldPlugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_view_mode_field_plugin\Plugin\EntityViewModeFieldPluginBase;

/**
 * Show Id in entity view mode.
 *
 * @EntityViewModeFieldPlugin(
 *   id = "entity_uuid",
 *   label = @Translation("UUID"),
 *   entity_type = {
 *     "node",
 *     "paragraph",
 *     "taxonomy_term",
 *     "user"
 *   }
 * )
 */
class ViewUuidField extends EntityViewModeFieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getValue(EntityInterface $entity) {
    return $entity->uuid();
  }

}
