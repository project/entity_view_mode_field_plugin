<?php

namespace Drupal\entity_view_mode_field_plugin\Plugin\EntityViewModeFieldPlugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_view_mode_field_plugin\Plugin\EntityViewModeFieldPluginBase;

/**
 * Show Id in entity view mode.
 *
 * @EntityViewModeFieldPlugin(
 *   id = "entity_id",
 *   label = @Translation("ID"),
 *   entity_type = {
 *     "node",
 *     "paragraph",
 *     "taxonomy_term",
 *     "commerce_product",
 *     "user"
 *   }
 * )
 */
class ViewIdField extends EntityViewModeFieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getValue(EntityInterface $entity) {
    return $entity->id();
  }

}
