<?php

namespace Drupal\entity_view_mode_field_plugin\Plugin\EntityViewModeFieldPlugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_view_mode_field_plugin\Plugin\EntityViewModeFieldPluginBase;

/**
 * Show alias in entity view mode.
 *
 * @EntityViewModeFieldPlugin(
 *   id = "path_alias_taxonomy",
 *   label = @Translation("URL Alias"),
 *   entity_type = {
 *     "taxonomy_term"
 *   }
 * )
 */
class ViewPathAliasTaxonomyField extends EntityViewModeFieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getValue(EntityInterface $entity) {
    return \Drupal::service('path_alias.manager')
      ->getAliasByPath('/taxonomy/term/' . $entity->id());
  }
}
