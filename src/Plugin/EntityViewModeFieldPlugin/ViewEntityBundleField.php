<?php

namespace Drupal\entity_view_mode_field_plugin\Plugin\EntityViewModeFieldPlugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_view_mode_field_plugin\Plugin\EntityViewModeFieldPluginBase;

/**
 * Show Bundle in entity view mode.
 *
 * @EntityViewModeFieldPlugin(
 *   id = "entity_bundle",
 *   label = @Translation("Bundle"),
 *   entity_type = {
 *     "node",
 *     "paragraph",
 *     "taxonomy_term",
 *     "user"
 *   }
 * )
 */
class ViewEntityBundleField extends EntityViewModeFieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getValue(EntityInterface $entity) {
    return $entity->bundle();
  }

}
