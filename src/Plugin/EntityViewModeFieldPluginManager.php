<?php

namespace Drupal\entity_view_mode_field_plugin\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Entity view mode field plugin plugin manager.
 */
class EntityViewModeFieldPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new EntityViewModeFieldPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/EntityViewModeFieldPlugin',
      $namespaces,
      $module_handler,
      'Drupal\entity_view_mode_field_plugin\Plugin\EntityViewModeFieldPluginInterface',
      'Drupal\entity_view_mode_field_plugin\Annotation\EntityViewModeFieldPlugin'
    );

    $this->alterInfo('entity_view_mode_field_plugin_entity_view_mode_field_plugin_info');
    $this->setCacheBackend($cache_backend, 'entity_view_mode_field_plugin_entity_view_mode_field_plugin_plugins');
  }

  /**
   * Get plugin definition by entity type.
   *
   * @param string $entity_type_id
   *   Entity type id.
   *
   * @return array
   *   Array of plugin definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getDefinitionsByEntityType($entity_type_id) {
    $plugin_definitions = parent::getDefinitions();
    $plugin_by_entity_type = [];
    foreach ($plugin_definitions as $key => $plugin) {
      if (!empty($plugin['entity_type']) && !in_array($entity_type_id, $plugin['entity_type'])) {
        continue;
      }
      $plugin_by_entity_type[$key] = $this->createInstance($key);
    }
    return $plugin_by_entity_type;
  }

}
